# README #

### What is this repository for? ###

Write a Golang program to Simulate a Gas Station:

1. You have 4 Gas Pumps
2. You have 10 Cars
3. Each car pulls up to an available pump for a 50 millisecond fill up, once full it gets back in line an waits for an available pump
4. Run the simulation for 30 seconds
5. Report at the end of the simulation the number of fill ups given to each car, and provided by each pump.
