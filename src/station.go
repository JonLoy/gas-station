package main

//import "fmt"

var CarQueue chan chan WorkRequest

func StartStation(nCars int) {
    CarQueue = make(chan chan WorkRequest, nCars)

    for i := 0; i < nCars; i++ {
        //fmt.Println("Starting car", i+1)
        car := NewCar(i+1, CarQueue)
        car.Start()
    }

    go func() {
        for {
            select {
            case work := <-WorkQueue:
               // fmt.Println("Received notice Pump", work.Number, "is open")
                go func() {
                    car := <-CarQueue

                //    fmt.Println("Dispatching work request Pump", work.Number)
                    car <- work
                }()
            }
       }
    }()
}

