package main

import (
//    "fmt"
    "time"
)

type Car struct {
    ID int
    FillUp chan WorkRequest
    CarQueue chan chan WorkRequest
}

func NewCar(id int, carQueue chan chan WorkRequest) Car {
    car := Car {
        ID: id,
        FillUp: make(chan WorkRequest),
        CarQueue: carQueue,
    }
    return car
}

func (car *Car) Start() {
    go func() {
        for {
            car.CarQueue <- car.FillUp

            select {
                case fillUp := <-car.FillUp:
                    //fmt.Printf("car%d: Hello, Pump %d!\n", car.ID, fillUp.Number)
                    time.Sleep(fillUp.Delay)
                    car.FillTank(fillUp)
             }
         }
     }()
}

func (car *Car) FillTank(fillUp WorkRequest){
    result := Receipt{car.ID, fillUp.Number}
    results <- result
    //fmt.Printf("car%d stopping fill-up\n", car.ID)
}
