package main

import (
    "fmt"
    "time"
)

func Embolden(str string) string {
    return "\033[1m" + str + "\033[0m"
}

func Italize(str string) string {
    return "\033[3m" + str + "\033[0m"
}

func Underline(str string) string {
    return "\033[4m" + str + "\033[0m"
}

func newStation(NCars int, NPumps int) map[int]map[int]int {
    station := make(map[int]map[int]int)
    for i := 0; i < NCars; i++ {
        station[i+1] = make(map[int]int)
        for j := 0; j < NPumps; j++ {
            station[i+1][j+1] = 0
        }
    }
    return station
}

var results = make(chan Receipt, 100)

func runSim(duration int){
    dur := time.Second * time.Duration(duration)
    timer := time.NewTimer(dur)
    NCars := 10
    NPumps := 4
    printSimConfig(dur, NCars, NPumps)
    StartStation(NCars)

    station := newStation(NCars, NPumps)

    for i := 1; i < NPumps+1; i++{
        Pump(i)
    }

    for {
        select {
        case  receipt := <-results:
            station[receipt.Car][receipt.Pump] += 1
            Pump(receipt.Pump)
        case <-timer.C:
            fmt.Println(Embolden("End of Simulation\n"))
            fmt.Println(Embolden("Compiling Report\n"))
            stationReport(station, NCars, NPumps)
            return
        }
    }
}

func printSimConfig(duration time.Duration, NCars int, NPumps int) {
    fmt.Println("----Simulation Configuration----")
    fmt.Println("Duration: ", duration)
    fmt.Printf("Modeling %d Cars and %d Gas Pumps\n", NCars, NPumps)
    fmt.Println("--------------------------------")
}

func stationReport(station map[int]map[int]int, NCars int, NPumps int) {
    var pumps = make([]int, NPumps)
    for i := 0; i < NCars; i++ {
        totalFillUps := 0
        header := fmt.Sprintf("Car %d", i+1)
        fmt.Println(Embolden(Underline(header)))
        for j := 0; j < NPumps; j++ {
            if station[i+1][j+1] > 0 {
                fmt.Printf("%d Fill up(s) At Pump %d \n", station[i+1][j+1], j+1)
                totalFillUps += station[i+1][j+1]
                pumps[j] += station[i+1][j+1]
            }
        }
        car_str := fmt.Sprintf("Car %d: Total Fill Ups At Station ", i+1)
        total_str:= fmt.Sprintf("%d\n", totalFillUps)
        fmt.Println(Embolden(car_str + Underline(total_str)))
     }
     for i := 0; i < NPumps; i++{
         fmt.Printf("Pump %d: Total Fill Ups %d\n", i+1, pumps[i])
     }
}

func main() {
    duration := 30
    runSim(duration)
}
