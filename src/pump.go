package main

import (
    //"fmt"
    "time"
)

var WorkQueue = make(chan WorkRequest, 100)

func Pump(number int){
    t := time.Millisecond * 50
    work := WorkRequest{number, t }
    WorkQueue <- work
    //fmt.Println("Pump", number, "open")
    return
}
