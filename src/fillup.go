package main

import "time"

type WorkRequest struct{
    Number int
    Delay time.Duration
}

type Receipt struct {
    Car  int 
    Pump int
}
